package com.groundgurus.day3.exercises;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Character {
    private String name;
    private int hp;
    private int attack;
    private int defense;
    private String affinity;
}
