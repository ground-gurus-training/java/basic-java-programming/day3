package com.groundgurus.day3.exercises;

public class Day3Exercise1 {
    public static void main(String[] args) {
        var blackFalcon = new Character("Black Falcon", 1_000, 500, 300, "Good");
        var wackyBill = new Character("Wacky Bill", 800, 300, 400, "Evil");

        var game = new Game();
        var winner = game.fight(blackFalcon, wackyBill);

        System.out.println(winner.getName() + " is the winner!");
    }
}
