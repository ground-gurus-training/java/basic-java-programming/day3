package com.groundgurus.day3.exercises;

import java.util.Random;

public class Game {
    public Character fight(Character character1, Character character2) {
        var whoIsFight = new Random();

        while (true) {
            var fighting = whoIsFight.nextInt(100) + 1;

            if (fighting % 2 == 0) {
                var charAttack = character1.getAttack();
                var enemyHp = character2.getHp();

                var damage = (int) (charAttack * whoIsFight.nextDouble());
                character2.setHp(enemyHp - damage);

                System.out.println(character1.getName() + " attacks " + character2.getName());
                System.out.println(character2.getName() + " received " + damage + " damage");
            } else {
                var charAttack = character2.getAttack();
                var enemyHp = character1.getHp();

                var damage = (int) (charAttack * whoIsFight.nextDouble());
                character1.setHp(enemyHp - damage);

                System.out.println(character2.getName() + " attacks " + character1.getName());
                System.out.println(character1.getName() + " received " + damage + " damage");
            }

            if (character1.getHp() <= 0) {
                return character2;
            } else if (character2.getHp() <= 0) {
                return character1;
            }
        }
    }
}
